#include "to_string.hpp"
#include "models/courier_service.hpp"
#include "models/medicine.hpp"

#include <iostream>
#include <stdexcept>
#include <string>

std::string ToString(const models::order::ProcessedOrder& order) { 
  return order.customer->last_name + ", " + order.customer->phone_number;
}

std::string ToString(const models::medicine::MedicineInfo& medicine_info) {
  return medicine_info.name + ", " + medicine_info.dosage +
         ", цена: " + std::to_string(medicine_info.price);
}

std::string ToString(
    const std::pair<std::string, models::medicine::StoredMedicine>& medicine) {
  if (!medicine.second.GetInfo()) {
    throw std::runtime_error("No medicine info");
  }
  return ToString(*medicine.second.GetInfo()) +
         ", в наличии: " + std::to_string(medicine.second.GetAmount());
}

std::string
ToString(const std::pair<int, double>& day_to_couriers_load) {
  std::string res = "День " + std::to_string(day_to_couriers_load.first + 1) + ":";
  res += " " + std::to_string(day_to_couriers_load.second);
  return res;
}
