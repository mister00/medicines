#include <QGroupBox>
#include <QFormLayout>

#include "main_result_frames.hpp"

namespace ui::result_frame {

ResultFrame::ResultFrame(QWidget *parent) : QFrame(parent) {
  setStyleSheet("color: black; background-color: white;");

  main_title_ = std::unique_ptr<QLabel>(new QLabel(parent));
  main_title_->setVisible(false);

  profit_frame_ = std::unique_ptr<ProfitFrame>(new ProfitFrame(parent));
  sold_frame_ = std::unique_ptr<SoldFrame>(new SoldFrame(parent));
  lost_due_writeoff_frame_ = std::unique_ptr<LossesFrame>(new LossesFrame(parent));
  lost_due_shortage_frame_ = std::unique_ptr<DiscardedFrame>(new DiscardedFrame(parent));
  lost_due_courier_frame_ = std::unique_ptr<NotDeliveredFrame>(new NotDeliveredFrame(parent));
  lost_due_discounts_frame_ = std::unique_ptr<NotEarnedFrame>(new NotEarnedFrame(parent));

  setVisible(false);
}

void ResultFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string &title) {
  main_title_->setText(QString::fromStdString(title));
  main_title_->setStyleSheet("color: black; background-color: white; border: 0px;");
  setVisible(true);
  main_title_->setVisible(true);

  sold_frame_->Start(model_ptr, "Продано:");
  profit_frame_->Start(model_ptr, "Прибыль:");
  lost_due_writeoff_frame_->Start(model_ptr, "Потеряно из-за списаний:");
  lost_due_shortage_frame_->Start(model_ptr, "Потеряно из-за недостатка:");
  lost_due_courier_frame_->Start(model_ptr, "Потеряно из-за недоставки:");
  lost_due_discounts_frame_->Start(model_ptr, "Потеряно из-за скидок:");

  if (!is_started_) {
    is_started_ = true;

    QFormLayout *layout = new QFormLayout;
    layout->addRow(main_title_.get());
    layout->addRow(sold_frame_->GetTitle(), sold_frame_->GetAmount());
    layout->addRow(profit_frame_->GetTitle(), profit_frame_->GetAmount());
    layout->addRow(lost_due_discounts_frame_->GetTitle(), lost_due_discounts_frame_->GetAmount());
    layout->addRow(lost_due_writeoff_frame_->GetTitle(), lost_due_writeoff_frame_->GetAmount());
    layout->addRow(lost_due_shortage_frame_->GetTitle(), lost_due_shortage_frame_->GetAmount());
    layout->addRow(lost_due_courier_frame_->GetTitle(), lost_due_courier_frame_->GetAmount());
    setLayout(layout);
  }

}

void ResultFrame::Step() {
  profit_frame_->Step();
  sold_frame_->Step();
  lost_due_writeoff_frame_->Step();
  lost_due_shortage_frame_->Step();
  lost_due_courier_frame_->Step();
  lost_due_discounts_frame_->Step();
}

ResultInfoFrame::ResultInfoFrame(QWidget *parent) : QFrame(parent) {
  title_ = std::unique_ptr<QLabel>(new QLabel(this));
  amount_ = std::unique_ptr<QLabel>(new QLabel(this));

  setVisible(false);
}

void ResultInfoFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr_, const std::string& title) {
  model_ptr = model_ptr_;

  title_->setText(QString::fromStdString(title));
  title_->setStyleSheet("color: black; border-radius: 0px; border: 0px;");
}

void ProfitFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ResultInfoFrame::Start(model_ptr, title);
  Fill(Count());
};

void SoldFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ResultInfoFrame::Start(model_ptr, title);
  Fill(Count());
};

void LossesFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ResultInfoFrame::Start(model_ptr, title);
  Fill(Count());
};

void DiscardedFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ResultInfoFrame::Start(model_ptr, title);
  Fill(Count());
};

void NotDeliveredFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ResultInfoFrame::Start(model_ptr, title);
  Fill(Count());
};

void NotEarnedFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ResultInfoFrame::Start(model_ptr, title);
  Fill(Count());
};

} // namespace ui::result_frame
