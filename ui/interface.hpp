#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include <memory>

#include <QMainWindow>
#include <QPushButton>
#include <QToolButton>
#include <QLabel>

#include <models/shop.hpp>
#include <ui/model_wrapper.hpp>
#include <ui/main_frame.hpp>

QT_BEGIN_NAMESPACE
namespace Ui {
class Interface;
}
QT_END_NAMESPACE

namespace ui {

class Interface : public QMainWindow, private ModelWrapper {
  Q_OBJECT

public:
  Interface(QWidget *parent = nullptr);
  ~Interface();

public slots:
  void Start();
  void Step();
  void Restart();

private:
  void ProcessException(const std::string &what);

private:
  Ui::Interface *ui;

  std::unique_ptr<main_frame::MainFrame> main_frame;

  std::unique_ptr<QPushButton> next_button;
  std::unique_ptr<QPushButton> restart_button;

  std::unique_ptr<QToolButton> exception_button;
};

} // namespace interface

#endif // INTERFACE_HPP
