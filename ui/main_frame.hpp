#ifndef MAINFRAME_HPP
#define MAINFRAME_HPP

#include <memory>

#include <QFrame>
#include <QGroupBox>
#include <QListWidget>
#include <QPushButton>
#include <QWidget>

#include <models/shop.hpp>
#include <ui/model_wrapper.hpp>
#include <ui/main_auxilary_frames.hpp>
#include <ui/main_result_frames.hpp>

namespace ui::main_frame {

class MainFrame : public QFrame, private ModelWrapper {
  Q_OBJECT
public:
  MainFrame(QWidget *parent = nullptr);
  ~MainFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr);
  void Step();
  void Restart();

private:
  std::unique_ptr<ListFrame> current_medicines;
  std::unique_ptr<ListFrame> last_orders;
  std::unique_ptr<ListFrame> couriers_info;

  std::unique_ptr<result_frame::ResultFrame> result_info;
};

} // namespace ui::main_frame

#endif // MAINFRAME_HPP
