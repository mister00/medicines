#ifndef ADDITIONALINFOFRAME_HPP
#define ADDITIONALINFOFRAME_HPP

#include "models/courier_service.hpp"
#include <memory>

#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QWidget>

#include <models/medicine.hpp>
#include <models/shop.hpp>
#include <models/order.hpp>
#include <stdexcept>
#include <ui/model_wrapper.hpp>
#include <to_string.hpp>

namespace ui::main_frame {

class ListFrame : public QFrame, protected ModelWrapper {
public:
  ListFrame(QWidget *parent = nullptr);
  ~ListFrame() = default;

  virtual void Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title);
  virtual void Step() = 0;

protected:
  template <typename DataT> void Fill(const DataT &data);

private:
  std::unique_ptr<QLabel> title_;
  std::unique_ptr<QListWidget> list_widget_;

  bool is_started_ = false;
};

class CurrentMedicinesFrame : public ListFrame {
public:
  CurrentMedicinesFrame(QWidget *parent = nullptr) : ListFrame(parent) {}
  ~CurrentMedicinesFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) override;
  void Step() override { Fill(Get()); }

private:
  std::unordered_map<std::string, models::medicine::StoredMedicine> Get() const {
    if (!model_ptr) {
      throw std::runtime_error("No model_ptr");
    }
    return model_ptr->storage;
  }
};

class LastOrdersFrame : public ListFrame {
public:
  LastOrdersFrame(QWidget *parent = nullptr) : ListFrame(parent) {}
  ~LastOrdersFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) override;
  void Step() override { Fill(Get()); }

private:
  std::vector<models::order::ProcessedOrder> Get() const {
    if (!model_ptr) {
      throw std::runtime_error("No model_ptr");
    }
    return model_ptr->latest_orders;
  }
};

class CouriersLoadFrame : public ListFrame {
public:
  CouriersLoadFrame(QWidget *parent = nullptr) : ListFrame(parent) {}
  ~CouriersLoadFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) override;
  void Step() override { Fill(Get()); }

private:
   models::courier_service::CourierLoad Get() const {
    if (!model_ptr) {
      throw std::runtime_error("No model_ptr");
    }
    return model_ptr->courier_service.GetCouriersLoad();
  }
};

template <typename DataT> void ListFrame::Fill(const DataT &data) {
  list_widget_->clear();
  for (const auto &one_bit : data) {
    list_widget_->addItem(QString::fromStdString(ToString(one_bit)));
  }
}

} // namespace ui::main_frame

#endif // ADDITIONALINFOFRAME_HPP
