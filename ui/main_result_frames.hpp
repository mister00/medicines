#pragma once

#include <memory>

#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QWidget>

#include <models/medicine.hpp>
#include <models/shop.hpp>
#include <models/order.hpp>
#include <ui/model_wrapper.hpp>
#include <to_string.hpp>

namespace ui::result_frame {

class ResultInfoFrame : public QFrame, protected ModelWrapper {
public:
  ResultInfoFrame(QWidget *parent = nullptr);
  ~ResultInfoFrame() = default;

  virtual void Start(std::shared_ptr<models::shop::Shop> model_ptr,
                     const std::string &title);
  virtual void Step() = 0;
  void ResetExperiment(std::shared_ptr<models::shop::Shop> model_ptr) { model_ptr = model_ptr; }

  QLabel* GetTitle() { return title_.get(); }
  QLabel* GetAmount() { return amount_.get(); }

protected:
  void Fill(const int amount) {
    amount_->setText(QString::fromStdString(std::to_string(amount)));
  }

private:
  std::unique_ptr<QLabel> title_;
  std::unique_ptr<QLabel> amount_;
};

class ProfitFrame : public ResultInfoFrame {
public:
  ProfitFrame(QWidget *parent = nullptr) : ResultInfoFrame(parent) {}
  ~ProfitFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr,
             const std::string &title) override;
  void Step() override { Fill(Count()); }

private:
  int Count() const {
    return model_ptr->cashbox.profit;
  }
};

class SoldFrame : public ResultInfoFrame {
public:
  SoldFrame(QWidget *parent = nullptr) : ResultInfoFrame(parent) {}
  ~SoldFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr,
             const std::string &title) override;
  void Step() override { Fill(Count()); }

private:
  int Count() const {
    if (!model_ptr) {
      throw std::runtime_error("No model_ptr");
    }
    return model_ptr->cashbox.sold;
  }
};

class LossesFrame : public ResultInfoFrame {
public:
  LossesFrame(QWidget *parent = nullptr) : ResultInfoFrame(parent) {}
  ~LossesFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr,
             const std::string &title) override;
  void Step() override { Fill(Count()); }

private:
  int Count() const {
    if (!model_ptr) {
      throw std::runtime_error("No model_ptr");
    }
    return model_ptr->cashbox.lost_due_writeoffs;
  }
};

class DiscardedFrame : public ResultInfoFrame {
public:
  DiscardedFrame(QWidget *parent = nullptr) : ResultInfoFrame(parent) {}
  ~DiscardedFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr,
             const std::string &title) override;
  void Step() override { Fill(Count()); }

private:
  int Count() const {
    if (!model_ptr) {
      throw std::runtime_error("No model_ptr");
    }
    return model_ptr->cashbox.lost_due_shortage;
  }
};

class NotDeliveredFrame : public ResultInfoFrame {
public:
  NotDeliveredFrame(QWidget *parent = nullptr) : ResultInfoFrame(parent) {}
  ~NotDeliveredFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr,
             const std::string &title) override;
  void Step() override { Fill(Count()); }

private:
  int Count() const {
    if (!model_ptr) {
      throw std::runtime_error("No model_ptr");
    }
    return model_ptr->cashbox.lost_due_courier;
  }
};

class NotEarnedFrame : public ResultInfoFrame {
public:
  NotEarnedFrame(QWidget *parent = nullptr) : ResultInfoFrame(parent) {}
  ~NotEarnedFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr,
             const std::string &title) override;
  void Step() override { Fill(Count()); }

private:
  int Count() const {
    if (!model_ptr) {
      throw std::runtime_error("No model_ptr");
    }
    return model_ptr->cashbox.lost_due_discounts;
  }
};

class ResultFrame : public QFrame {
public:
  ResultFrame(QWidget *parent = nullptr);
  ~ResultFrame() = default;

  void Start(std::shared_ptr<models::shop::Shop> model_ptr,
                     const std::string &title);
  void Step();

private:
  std::unique_ptr<QLabel> main_title_;

  std::unique_ptr<ResultInfoFrame> profit_frame_;
  std::unique_ptr<ResultInfoFrame> sold_frame_;
  std::unique_ptr<ResultInfoFrame> lost_due_writeoff_frame_;
  std::unique_ptr<ResultInfoFrame> lost_due_shortage_frame_;
  std::unique_ptr<ResultInfoFrame> lost_due_courier_frame_;
  std::unique_ptr<ResultInfoFrame> lost_due_discounts_frame_;

  bool is_started_ = false;
};

} // namespace ui::result_frame
