#ifndef EXPERIMENTWRAPPER_HPP
#define EXPERIMENTWRAPPER_HPP

#include <memory>

#include <models/shop.hpp>

namespace ui {

struct ModelWrapper {
protected:
  std::shared_ptr<models::shop::Shop> model_ptr;
};

} // namespace interface

#endif // EXPERIMENTWRAPPER_HPP
