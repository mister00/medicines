#include "main_frame.hpp"

#include <QGridLayout>

#include <constants.hpp>

namespace ui::main_frame {

const std::string kStore = "Склад";
const std::string kCashbox = "Касса";
const std::string kOrders = "Текущие заказы";
const std::string kCourierService = "Курьерская служба";

MainFrame::MainFrame(QWidget *parent) : QFrame(parent) {
  setGeometry(positions::kMainX, positions::kMainY, sizes::kMainWidth,
              sizes::kMainHeight);
  setFrameStyle(QFrame::Panel);

  current_medicines =
      std::unique_ptr<CurrentMedicinesFrame>(new CurrentMedicinesFrame(this));
  last_orders = std::unique_ptr<LastOrdersFrame>(new LastOrdersFrame(this));
  couriers_info =
      std::unique_ptr<CouriersLoadFrame>(new CouriersLoadFrame(this));

  result_info = std::unique_ptr<result_frame::ResultFrame>(new result_frame::ResultFrame(this));

  QGridLayout *main_layout = new QGridLayout;
  main_layout->addWidget(current_medicines.get(), 0, 0, 2, 1);
  main_layout->addWidget(last_orders.get(), 0, 1, 1, 1);
  main_layout->addWidget(result_info.get(), 0, 2, 2, 1);
  main_layout->addWidget(couriers_info.get(), 1, 1, 1, 1);
  setLayout(main_layout);
}

void MainFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr) {
  current_medicines->Start(model_ptr, kStore);
  last_orders->Start(model_ptr, kOrders);
  couriers_info->Start(model_ptr, kCourierService);

  result_info->Start(model_ptr, kCashbox);
}

void MainFrame::Step() {
  current_medicines->Step();
  last_orders->Step();
  couriers_info->Step();
  result_info->Step();
}

void MainFrame::Restart() {
}

} // namespace ui::main_frame
