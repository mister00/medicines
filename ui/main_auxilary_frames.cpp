#ifndef ADDITIONALINFOFRAME_CPP
#define ADDITIONALINFOFRAME_CPP

#include <QGroupBox>
#include <QVBoxLayout>

#include "main_auxilary_frames.hpp"

namespace ui::main_frame {

ListFrame::ListFrame(QWidget *parent) : QFrame(parent) {
  list_widget_ = std::unique_ptr<QListWidget>(new QListWidget(this));
  list_widget_->setVisible(false);

  title_ = std::unique_ptr<QLabel>(new QLabel(this));

  setStyleSheet("background-color: white;");

  setVisible(false);
}

void ListFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr_, const std::string& title) {
  model_ptr = model_ptr_;
  setVisible(true);
  list_widget_->setStyleSheet("color: black; border-radius: 0px; border: 0px;");
  list_widget_->setVisible(true);

  title_->setText(QString::fromStdString(title));
  title_->setStyleSheet("color: black; border-radius: 0px; border: 0px;");

  if (!is_started_) {
    is_started_ = true;

    QVBoxLayout *controlsLayout = new QVBoxLayout;
    controlsLayout->addWidget(title_.get());
    controlsLayout->addWidget(list_widget_.get());
    setLayout(controlsLayout);
  }

}

void CurrentMedicinesFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ListFrame::Start(model_ptr, title);
  Fill(Get());
};

void CouriersLoadFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ListFrame::Start(model_ptr, title);
  Fill(Get());
};

void LastOrdersFrame::Start(std::shared_ptr<models::shop::Shop> model_ptr, const std::string& title) {
  ListFrame::Start(model_ptr, title);
  Fill(Get());
};

} // namespace ui::main_frame

#endif // ADDITIONALINFOFRAME_CPP
