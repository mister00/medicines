#include "interface.hpp"
#include "./ui_interface.h"
#include "constants.hpp"
#include "models/courier_service.hpp"
#include "models/medicine.hpp"
#include "models/order.hpp"
#include "models/storage.hpp"
#include <file_reader.hpp>

#include <iostream>

#include <QApplication>
#include <QHBoxLayout>
#include <QProgressBar>
#include <QSlider>

#include <models/shop.hpp>
#include <stdexcept>

namespace ui {

Interface::Interface(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::Interface) {
  ui->setupUi(this);
  setFixedSize(sizes::kInterfaceWidth, sizes::kInterfaceHeight);
  setWindowTitle("Система контроля доставки и количества лекарств");

  main_frame =
      std::unique_ptr<main_frame::MainFrame>(new main_frame::MainFrame(this));

  next_button = std::unique_ptr<QPushButton>(new QPushButton(this));
  next_button->setText("Следующий день");
  next_button->setGeometry(
      positions::kMainX, positions::kMainY + sizes::kMainHeight,
      sizes::kMainWidth / 2, sizes::kInterfaceHeight / 12);
  connect(next_button.get(), SIGNAL(clicked()), this, SLOT(Step()));
  
  restart_button = std::unique_ptr<QPushButton>(new QPushButton(this));
  restart_button->setText("Начать заново");
  restart_button->setGeometry(
      positions::kMainX + sizes::kMainWidth / 2, positions::kMainY + sizes::kMainHeight,
      sizes::kMainWidth / 2, sizes::kInterfaceHeight / 12);
  connect(restart_button.get(), SIGNAL(clicked()), this, SLOT(Restart()));

  exception_button = std::unique_ptr<QToolButton>(new QToolButton(this));
  exception_button->setVisible(false);

  connect(exception_button.get(), SIGNAL(clicked()), QApplication::instance(),
          SLOT(quit()));
}

Interface::~Interface() { delete ui; }

std::shared_ptr<models::shop::Shop> SetUpModel() {
  const auto couriers_amount = kCouriersAmount;

  auto initial_medicines = ReadMedicines(kMaxMedicineAmount);

  auto customers = ReadPossibleCustomers();

  std::vector<models::medicine::MedicineInfoPtr> medicine_infos;
  medicine_infos.reserve(initial_medicines.size());
  models::storage::Storage storage;
  storage.reserve(initial_medicines.size());
  for (const auto& medicine: initial_medicines) {
    medicine_infos.push_back(medicine.info);
    if (!medicine.info) {
      throw std::runtime_error("No medicine info");
    }
    storage[medicine.info->id] = models::medicine::StoredMedicine{.info = medicine.info, .made_at_queue={}};
    storage[medicine.info->id].Deposit(medicine.amount, 0);
  }

  auto regular_orders = ReadRegularOrders(customers, medicine_infos);

  return std::shared_ptr<models::shop::Shop>{new models::shop::Shop{
    .medicine_infos = medicine_infos,
    .storage = std::move(storage),
    .courier_service = models::courier_service::CourierService(couriers_amount),
    .customer_base = models::order::CustomerBase{std::move(customers), std::move(regular_orders), std::move(medicine_infos)},
    .extra_charge_percent = kExtraChargePercent
  }};
}

void Interface::Start() {
  try {
    model_ptr = SetUpModel();
    main_frame->Start(model_ptr);
  } catch (std::exception &exc) {
    ProcessException(exc.what());
  }
}

void Interface::Step() {
  if (!model_ptr) {
    ProcessException("No model_ptr in Step");
  }
  try {
    model_ptr->Step();
    main_frame->Step();
  } catch (std::exception &exc) {
    ProcessException(exc.what());
  }
}

void Interface::Restart() {
  main_frame->setVisible(true);
  model_ptr.reset();
  main_frame->Restart();
  Start();
}

void Interface::ProcessException(const std::string &what) {
  std::cout << "Error: " << what << std::endl;

  main_frame.reset();

  auto label = new QLabel(QString::fromStdString(what), exception_button.get());
  label->setWordWrap(true);
  auto layout = new QHBoxLayout(exception_button.get());
  layout->addWidget(label, 0, Qt::AlignCenter);

  exception_button->setGeometry(
      sizes::kInterfaceWidth / 2 - sizes::kMainStartRad,
      sizes::kInterfaceHeight / 2 - sizes::kMainStartRad,
      sizes::kMainStartRad * 2, sizes::kMainStartRad * 2);
  exception_button->setStyleSheet(
      "color: black; background-color: white; border: 0px groove black; "
      "border-radius: 130px; font-size: 23px;");
  exception_button->setVisible(true);
}

} // namespace interface
