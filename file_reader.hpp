#ifndef INTERFACE_FILEREADER_HPP
#define INTERFACE_FILEREADER_HPP

#include <fstream>
#include <sstream>
#include <unordered_map>
#include <vector>

#include <models/medicine.hpp>
#include <models/order.hpp>

namespace ui {

class FileReader {
public:
  FileReader(const std::string &path_to_file);
  ~FileReader() { input_.close(); }

  bool ReadNextLine();
  std::string GetNextValue(char delimiter = ';');

  inline bool IsLineOver() { return current_line_.eof(); }
  inline bool HasNext() { return !IsLineOver(); }

private:
  std::ifstream input_;
  std::stringstream current_line_;
};

models::medicine::Medicines ReadMedicines(const int max_amount);
std::vector<models::order::CustomerPtr> ReadPossibleCustomers();
std::vector<models::order::RegularOrder>
ReadRegularOrders(const std::vector<models::order::CustomerPtr> &customers,
                  const std::vector<models::medicine::MedicineInfoPtr> &medicines);

} // namespace interface

#endif // INTERFACE_FILEREADER_HPP
