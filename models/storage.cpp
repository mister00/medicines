#include "storage.hpp"
#include "models/medicine.hpp"
#include "models/order.hpp"

namespace models::storage {

order::ProcessedOrders Storage::ProcessOrders(const order::Orders& orders, int current_day) {
  order::ProcessedOrders result;
  for (const auto& order: orders) {
    order::ProcessedOrder processed_order {.customer = order.customer};
    for (const auto& medicine: order) {
      auto& stored_medicine = at(medicine.info->id);
      processed_order.push_back(at(medicine.info->id).Withdraw(medicine.amount, current_day));
    }
    if (!processed_order.empty()) {
      result.push_back(std::move(processed_order));
    };
  }
  return result;
}

medicine::Medicines Storage::WriteOff(int current_day) {
  medicine::Medicines result;
  for (auto& [_, medicine]: *this) {
    result.push_back(medicine.WriteOff(current_day));
  }
  UpdateFactoryOrders(current_day);
  return result;
}

void Storage::UpdateFactoryOrders(int current_day) {
  for (auto order_ptr = factory_orders.begin(); order_ptr != factory_orders.end(); order_ptr++) {
    auto& [delivery_date, order] = *order_ptr;
    if (delivery_date <= current_day) {
      this->at(order.info->id).Deposit(order.amount, current_day);
    } else {
      break;
    }
  }
  factory_orders.erase(current_day);
  for (auto& [_, medicine]: *this) {
    if (medicine.GetAmount() < kReorderThreshold) {
      int delivery_date = request_eta(random_generator) + current_day;
      factory_orders.insert(std::make_pair(delivery_date, medicine::Medicine{.amount=kReorderThreshold, .info=medicine.info}));
    }
  }
}

} // namespace models::storage
