#include "order.hpp"
#include "models/medicine.hpp"

#include <constants.hpp>

namespace models::order {

Orders CustomerBase::GenerateOrders(int current_day, int extra_charge_percent) {
  Orders result;
  for (auto& order: regular_orders) {
    if (order.IsToday(current_day)) {
      order.Prepare(current_day);
      result.push_back(order);
    }
  }

  const int default_amount =
      (kMaxPercent - extra_charge_percent) * this->size() / kMaxPercent;
  const double mult =
      orders_amount_mult(random_generator) / kOrderAmountMultMax;
  int orders_amount = default_amount * mult;

  std::vector<CustomerPtr> ordering_customers;
  ordering_customers.reserve(orders_amount);
  std::sample(this->begin(), this->end(),
              std::back_inserter(ordering_customers), orders_amount,
              random_generator);

  for (const auto& customer: ordering_customers) {
    order::Order order{.customer = customer};

    bool is_empty = true;
    for (const auto &medicine_info : medicine_infos) {
      if (buy_indicator(random_generator)) {
        is_empty = false;
        const int ordered_amount = buy_amount(random_generator);
        order.push_back(medicine::Medicine{.amount = ordered_amount, .info = medicine_info});
      }
    }
    if (!order.empty()) {
      result.push_back(order);
    }
  }

  return result;
}

} // namespace models::order
