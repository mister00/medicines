#pragma once

#include <memory>
#include <random>
#include <optional>
#include <unordered_map>
#include <utility>
#include <vector>

#include <models/medicine.hpp>

namespace models::order {

struct DiscountCard {
  std::string number;
  const int card_discount_percent;
  const int big_cost_discount_percent;
  const int extra_charge_percent;
  const int initial_discount;
};

struct Customer {
  std::string last_name;
  std::string phone_number;
  std::string address;
  std::optional<DiscountCard> discount_card;
};

using CustomerPtr = std::shared_ptr<Customer>;

struct Order: public medicine::Medicines {
  CustomerPtr customer;
};

struct ProcessedOrder: public medicine::PricedMedicines {
  CustomerPtr customer;
};

using Orders = std::vector<Order>;

using ProcessedOrders = std::vector<ProcessedOrder>;

class RegularOrder : public Order {
public:
  RegularOrder() = default;
  RegularOrder(const Order &order, const int frequency): Order(order), frequency_(frequency) {};

  bool IsToday(const int current_day) const {
    return last_order_date_ == 0 ||
           last_order_date_ + frequency_ == current_day;
  }
  inline void Prepare(const int current_day) { last_order_date_ = current_day; }

private:
  int frequency_;
  int last_order_date_{0};
};


class CustomerBase: public std::vector<CustomerPtr> {
  public:
    CustomerBase() = default;
    CustomerBase(std::vector<CustomerPtr> customer_ptr, std::vector<RegularOrder> regular_orders, std::vector<medicine::MedicineInfoPtr> medicine_infos):
      std::vector<CustomerPtr>(std::move(customer_ptr)), regular_orders(std::move(regular_orders)), medicine_infos(std::move(medicine_infos)) {}
    Orders GenerateOrders(int current_day, int extra_charge_percent);

  private:
    std::vector<RegularOrder> regular_orders;
    std::vector<medicine::MedicineInfoPtr> medicine_infos;

    mutable std::mt19937 random_generator{std::random_device{}()};
    mutable std::uniform_int_distribution<std::mt19937::result_type> buy_amount{
        kBuyAmountMin, kBuyAmountMax};
    mutable std::uniform_real_distribution<double> orders_amount_mult{
        kOrderAmountMultMin, kOrderAmountMultMax};
    mutable std::discrete_distribution<> buy_indicator{
        kMaxPercent - kBuyPercent, kBuyPercent};
};

} // namespace models::order
