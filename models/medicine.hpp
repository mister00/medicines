#pragma once

#include <queue>
#include <string>

#include <constants.hpp>

namespace models::medicine {

struct MedicineInfo {
  std::string id;
  std::string name;
  std::string type;
  std::string group;
  std::string dosage;
  int price;
  int life_span;

  bool IsDiscounted(int age);
  bool IsExpired(int age);
};

using MedicineInfoPtr = std::shared_ptr<MedicineInfo>;

struct Medicine {
  int amount;
  MedicineInfoPtr info;
};

struct PricedMedicine: public Medicine {
  int amount_discounted;

  int GetPrice() const;
};

using Medicines = std::vector<medicine::Medicine>;
using PricedMedicines = std::vector<medicine::PricedMedicine>;

struct StoredMedicine {
  std::queue<int> made_at_queue;
  MedicineInfoPtr info;

  PricedMedicine Withdraw(int count, int current_day);
  void Deposit(int count, int current_day);
  Medicine WriteOff(int current_day);
  int GetAmount() const;
  MedicineInfoPtr GetInfo() const;
};

} // namespace models::medicine
