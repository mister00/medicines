#pragma once

#include <map>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <models/medicine.hpp>
#include <models/order.hpp>
#include <constants.hpp>

namespace models::storage {

class Storage: public std::unordered_map<std::string, medicine::StoredMedicine> {
 public:
  order::ProcessedOrders ProcessOrders(const order::Orders& orders, int current_day);
  medicine::Medicines WriteOff(int current_day);

 private:
  void UpdateFactoryOrders(int current_day);
  std::multimap<int, medicine::Medicine> factory_orders{};

  mutable std::mt19937 random_generator{std::random_device{}()};
  mutable std::uniform_int_distribution<std::mt19937::result_type> request_eta{
        kRequestEtaMin, kRequestEtaMax};
};

} // namespace models::storage
