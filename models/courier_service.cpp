#include "courier_service.hpp"
#include <constants.hpp>

namespace models::courier_service {

order::ProcessedOrders CourierService::ProcessOrders(const order::ProcessedOrders& orders, int current_day) {
  order::ProcessedOrders result;
  for (const auto& order: orders) {
    if (order.empty()) {
      continue;
    }
    if (result.size() > kMaxCourierOrders * couriers_amount) {
      break;
    }
    result.push_back(order);
  }
  couriers_load[current_day] = static_cast<double>(result.size()) / couriers_amount;
  return result;
}

} // namespace models::courier_service
