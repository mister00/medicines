#include "medicine.hpp"

namespace models::medicine {

bool MedicineInfo::IsDiscounted(int age) {
  return age >= life_span - 30;
}

bool MedicineInfo::IsExpired(int age) {
  return age > life_span;
}

int PricedMedicine::GetPrice() const {
  return (amount - amount_discounted) * info->price + amount_discounted * info->price / 2;
}

PricedMedicine StoredMedicine::Withdraw(int count, int current_day) {
  PricedMedicine result {Medicine{.amount = 0, .info = info}, 0};
  for (int i = 0; i < count; i++) {
    if (made_at_queue.empty()) {
      break;
    }
    auto age = current_day - made_at_queue.back();
    made_at_queue.pop();
    if (info->IsDiscounted(age)) {
      result.amount_discounted += 1;
    }
    result.amount += 1;
  }
  return result;
}

void StoredMedicine::Deposit(int count, int current_day) {
  for (int i = 0; i < count; i++) {
    made_at_queue.push(current_day);
  }
}
  
Medicine StoredMedicine::WriteOff(int current_day) {
  Medicine result{.amount=0, .info=info};
  while (info->IsExpired(current_day - made_at_queue.back())) {
    result.amount++;
  }
  return result;
}

int StoredMedicine::GetAmount() const {
  return made_at_queue.size();
}

MedicineInfoPtr StoredMedicine::GetInfo() const {
  return info;
}

} // namespace models::medicine
