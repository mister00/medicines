#pragma once

#include <optional>
#include <string>
#include <utility>

#include <models/order.hpp>
#include <models/storage.hpp>

namespace models::courier_service {

using CourierLoad = std::map<int, double>;

class CourierService {
public:
  CourierService() = default;
  CourierService(int couriers_amount): couriers_amount(couriers_amount){}
  order::ProcessedOrders ProcessOrders(const order::ProcessedOrders& orders, int current_day);

  CourierLoad GetCouriersLoad() const {
    return couriers_load;
  }

private:
  int couriers_amount;

  CourierLoad couriers_load = {};
};

} // namespace models::courier_service
