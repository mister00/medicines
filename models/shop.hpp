#pragma once

#include "order.hpp"
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <constants.hpp>
#include <models/courier_service.hpp>
#include <models/medicine.hpp>
#include <models/order.hpp>
#include <models/storage.hpp>

namespace models::shop {

struct Cashbox {
  int sold;
  int profit;
  int lost_due_discounts;
  int lost_due_writeoffs;
  int lost_due_shortage;
  int lost_due_courier;

  void ProcessOrders(const order::Orders& orders, const order::ProcessedOrders orders_after_storage, const order::ProcessedOrders orders_after_courier, int extra_charge_percent);
  void ProcessWriteOffs(const medicine::Medicines& medicines);
};

class Shop {
 public:
  void Step();

  std::vector<medicine::MedicineInfoPtr> medicine_infos;
  storage::Storage storage;
  courier_service::CourierService courier_service;
  order::CustomerBase customer_base;
  int extra_charge_percent;
  int current_day{0};
  order::ProcessedOrders latest_orders{};
  Cashbox cashbox{};
};

} // namespace models
