#include "shop.hpp"

#include <algorithm>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <models/courier_service.hpp>
#include <models/medicine.hpp>
#include <models/order.hpp>
#include <models/storage.hpp>

namespace models::shop {

void Cashbox::ProcessOrders(const order::Orders& orders, const order::ProcessedOrders orders_after_storage, const order::ProcessedOrders orders_after_courier, int extra_charge_percent) {
  int order_price = 0;
  int order_after_storage_price = 0;
  int sold_order_price = 0;
  for (const auto& order: orders_after_courier) {
    for (const auto& medicine: order) {
      sold_order_price += medicine.amount * medicine.info->price;
      profit += medicine.GetPrice() * extra_charge_percent / 100;
      sold += medicine.amount;
      lost_due_discounts+= medicine.amount_discounted * medicine.info->price / 2;
      profit -= medicine.amount_discounted * medicine.info->price / 2;
    }
  }  
  for (const auto& order: orders_after_storage) {
    for (const auto& medicine: order) {
      order_after_storage_price += medicine.amount * medicine.info->price;
    }
  }
   for (const auto& order: orders) {
    for (const auto& medicine: order) {
      order_price += medicine.amount * medicine.info->price;
    }
  }
  lost_due_shortage += order_price - order_after_storage_price;
  lost_due_courier += order_after_storage_price - sold_order_price;
}

void Cashbox::ProcessWriteOffs(const medicine::Medicines& medicines) {
  for (const auto& medicine: medicines) {
    lost_due_writeoffs += medicine.amount * medicine.info->price;
    profit -= medicine.amount * medicine.info->price;
  }
}

void Shop::Step() {
  auto writeoffs = storage.WriteOff(current_day);
  cashbox.ProcessWriteOffs(writeoffs);

  auto orders = customer_base.GenerateOrders(current_day, extra_charge_percent);
  auto processed_in_storage = storage.ProcessOrders(orders, current_day);
  auto processed_in_courier = courier_service.ProcessOrders(processed_in_storage, current_day);
  cashbox.ProcessOrders(orders, processed_in_storage, processed_in_courier, extra_charge_percent);
  latest_orders = processed_in_courier;
  current_day++;
};

} // namespace models
