#pragma once

#include <utility>

#include <models/courier_service.hpp>
#include <models/medicine.hpp>
#include <models/order.hpp>
#include <models/storage.hpp>

std::string ToString(const models::order::ProcessedOrder& order);

std::string ToString(const models::medicine::MedicineInfo& medicine_info);

std::string
ToString(const std::pair<std::string, models::medicine::StoredMedicine>& medicines);

std::string
ToString(const std::pair<int, double>& day_to_couriers_load);
