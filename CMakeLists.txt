cmake_minimum_required(VERSION 3.5)

project(Medicines VERSION 0.1 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_PREFIX_PATH "/opt/homebrew/bin/qt-cmake")

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets REQUIRED)

set(PROJECT_SOURCES
        main.cpp
        constants.hpp
        file_reader.cpp
        file_reader.hpp
        to_string.cpp
        to_string.hpp
        models/shop.cpp
        models/shop.hpp
        models/courier_service.cpp
        models/courier_service.hpp
        models/storage.cpp
        models/storage.hpp
        models/order.cpp
        models/order.hpp
        models/medicine.cpp
        models/medicine.hpp
        ui/interface.cpp
        ui/interface.hpp
        ui/main_auxilary_frames.cpp
        ui/main_auxilary_frames.hpp
        ui/main_result_frames.cpp
        ui/main_result_frames.hpp
        ui/main_frame.cpp
        ui/main_frame.hpp
        ui/model_wrapper.hpp
        ui/interface.ui
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(Medicines
        MANUAL_FINALIZATION
        ${PROJECT_SOURCES}
    )
# Define target properties for Android with Qt 6 as:
#    set_property(TARGET Drugs APPEND PROPERTY QT_ANDROID_PACKAGE_SOURCE_DIR
#                 ${CMAKE_CURRENT_SOURCE_DIR}/android)
# For more information, see https://doc.qt.io/qt-6/qt-add-executable.html#target-creation
else()
    if(ANDROID)
        add_library(Medicines SHARED
            ${PROJECT_SOURCES}
        )
# Define properties for Android with Qt 5 after find_package() calls as:
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
    else()
        add_executable(Medicines
            ${PROJECT_SOURCES}
        )
    endif()
endif()

target_link_libraries(Medicines PRIVATE Qt${QT_VERSION_MAJOR}::Widgets)

set_target_properties(Medicines PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(Medicines)
endif()
