#pragma once

#include <QString>

namespace models {

namespace storage {

constexpr int kRequestEtaMin = 1;
constexpr int kRequestEtaMax = 3;
constexpr int kReorderThreshold = 20;

}

namespace shop {

constexpr int kBigCostAmount = 1000;
constexpr int kRegularDiscountPercent = 5;
constexpr int kMaxDiscountPercent = 9;

}

namespace order {

constexpr int kBuyAmountMin = 1;
constexpr int kBuyAmountMax = 3;
constexpr int kOrderAmountMultMin = 6;
constexpr int kOrderAmountMultMax = 10;
constexpr double kMaxPercent = 100.0;
constexpr int kBuyPercent = 30;

} // namespace order

namespace courier_service {

constexpr int kMaxCourierOrders = 15;

} // namespace courier_service

} // namespace models

namespace ui {

constexpr int kExtraChargePercent = 20;
constexpr int kCouriersAmount = 1;
constexpr int kMaxMedicineAmount = 30;

namespace sizes {

constexpr double kInterfaceWidth = 1000;
constexpr double kInterfaceHeight = 600;

constexpr double kMainWidth = kInterfaceWidth - (kInterfaceWidth / 20);
constexpr double kMainHeight = kInterfaceHeight * 3 / 4;

constexpr double kMainStartRad = kMainHeight / 3;

constexpr double kResultHeight = kMainHeight * 3 / 5;
constexpr double kResultWidth = kMainWidth / 2;

} // namespace sizes

namespace positions {

constexpr double kMainX = (sizes::kInterfaceWidth - sizes::kMainWidth) / 2;
constexpr double kMainY = sizes::kInterfaceHeight / 18;

} // namespace positions

} // namespace ui
