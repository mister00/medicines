#include <ui/interface.hpp>

#include <QApplication>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  ui::Interface w;
  w.Start();
  w.show();
  return a.exec();
}
