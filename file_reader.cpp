#include "file_reader.hpp"
#include "models/medicine.hpp"
#include "models/order.hpp"

#include <iostream>

namespace ui {

namespace {

namespace medicine = models::medicine;
namespace order = models::order;

} // namespace

FileReader::FileReader(const std::string &path_to_file) : input_(path_to_file) {
  if (!input_.is_open()) {
    throw std::runtime_error("failed to open " + path_to_file);
  }
}

bool FileReader::ReadNextLine() {
  std::string line;
  if (!getline(input_, line)) {
    return false;
  }

  current_line_.clear();
  current_line_.str(std::string{});
  current_line_.str(line);
  return true;
}

std::string FileReader::GetNextValue(char delimiter) {
  std::string result;
  if (!getline(current_line_, result, delimiter)) {
    throw std::runtime_error("No more values in string");
  }

  return result;
}

models::medicine::Medicines ReadMedicines(const int max_amount) {
  FileReader reader(
      "/Users/nmolotilov/Medicines/input_files/initial_medicines.txt");

  models::medicine::Medicines result;
  result.reserve(max_amount);

  if (!reader.ReadNextLine()) {
    return result;
  }

  while (reader.ReadNextLine()) {
    medicine::MedicineInfoPtr medicine_info = std::make_shared<medicine::MedicineInfo>(medicine::MedicineInfo{
      .id = reader.GetNextValue(),
      .name = reader.GetNextValue(),
      .type = reader.GetNextValue(),
      .group = reader.GetNextValue(),
      .dosage = reader.GetNextValue(),
      .price = std::stoi(reader.GetNextValue()),
      .life_span = std::stoi(reader.GetNextValue())
    });

    int amount = std::stoi(reader.GetNextValue());

    if (reader.HasNext()) {
      throw std::runtime_error(
          "initial_medicines.txt is in the wrong format: extra values in line");
    }

    result.emplace_back(medicine::Medicine{.info = medicine_info, .amount = amount});
    if (result.size() == max_amount) {
      break;
    }
  }
  return result;
}

std::vector<order::CustomerPtr> ReadPossibleCustomers() {
  FileReader reader(
      "/Users/nmolotilov/Medicines/input_files/possible_customers.txt");

  std::vector<order::CustomerPtr> result;

  if (!reader.ReadNextLine()) {
    return result;
  }

  while (reader.ReadNextLine()) {
    order::CustomerPtr customer = std::make_shared<order::Customer>(order::Customer{
      .last_name = reader.GetNextValue(),
      .phone_number = reader.GetNextValue(),
      .address = reader.GetNextValue(),
      .discount_card = reader.HasNext() ? std::make_optional(models::order::DiscountCard{
        .number = reader.GetNextValue()
      }) : std::nullopt
    });

    if (reader.HasNext()) {
      throw std::runtime_error("possible_customers.txt is in the wrong format: "
                                "extra values in line");
    }

    result.push_back(customer);
  }
  return result;
}

std::vector<order::RegularOrder>
ReadRegularOrders(const std::vector<order::CustomerPtr> &customers,
                  const std::vector<medicine::MedicineInfoPtr> &medicines) {
  FileReader reader(
      "/Users/nmolotilov/Medicines/input_files/regular_orders.txt");

  std::vector<order::RegularOrder> result;

  if (!reader.ReadNextLine()) {
    return result;
  }

  while (reader.ReadNextLine()) {
    const auto frequency = std::stoi(reader.GetNextValue());
    const auto customer_ind = std::stoi(reader.GetNextValue());
    if (customer_ind >= customers.size()) {
      throw std::runtime_error("Not enough customers for customer_ind " +
                               std::to_string(customer_ind));
    }
    order::Order order{.customer=customers[customer_ind]};
    while (reader.HasNext()) {
      const auto medicines_ind = std::stoi(reader.GetNextValue());
      if (medicines_ind >= medicines.size()) {
        throw std::runtime_error("Not enough medicines for medicines_ind " +
                                 std::to_string(medicines_ind));
      }
      const auto amount = std::stoi(reader.GetNextValue());
      order.push_back(medicine::Medicine{.info = medicines[medicines_ind], .amount = amount});

    }
    result.emplace_back(order, frequency);
  }
  return result;
}

} // namespace interface
